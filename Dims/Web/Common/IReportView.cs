﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.UI;
using Dims.DAL;
using Dims.Web.Controls;

namespace Dims.Web.Common
{

    public interface IReportView : IListView
    {
        string ReportName { get; }
        bool ThrowException { get; set; }
        string ReportNames { get; set; }
        int CurrentReportNumber { set; }
        bool Visible { get; set; }
    }
}
