﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace Dims.Web.Common
{
    public interface IFilter
    {

        event EventHandler Apply;
    }

    public interface IFilter<TFilterEntity> : IFilter
    {
        void InitData();
        void Fill(TFilterEntity filter);
        void Init(TFilterEntity filter);
    }

    
   

   

}
