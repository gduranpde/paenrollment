﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;
using System.Web.UI;

namespace Dims.Web.Controls
{
    [Themeable(true)]
    public class PagerControl:ControlWithPresenter , IPager
    {

        public event EventHandler ParamsChange;

        private void CallParamsChange()
        {
            if (ParamsChange != null)
            {
                ParamsChange(this,null);
            }


            if (Presenter != null)
            {
                Presenter.RebindData = true;
            }
        }

        protected override void OnInit(EventArgs e)
        {

            Presenter.InitPager(this);

            base.OnInit(e);
        }

        public bool CanPrev
        {
            get
            {
                return TotalCount > 0 && PageIndex > 0;
            }
        }

        public bool CanNext
        {
            get
            {
                return TotalCount > 0 && PageIndex < MaxPageIndex;
            }
        }


        public virtual int PageIndex
        {
            get
            {
                return Convertor.ToInt(this.ViewState["PageIndex"]);
            }
            set
            {
                var v = value;

                if (v > MaxPageIndex)
                {
                    v = MaxPageIndex;
                }

                if (v < 0)
                {
                    v = 0;
                }
              
                

                this.ViewState["PageIndex"] = v;
                CallParamsChange();
                
            }
        }

        public virtual int PageSize
        {
            get
            {
                return Convertor.ToInt(this.ViewState["PageSize"]);
            }
            set
            {
                this.ViewState["PageSize"] = value;
                CallParamsChange();
            }
        }

        public virtual int TotalCount
        {
            get
            {
                return Convertor.ToInt(this.ViewState["TotalCount"]);
            }
            set
            {
                this.ViewState["TotalCount"] = value;
                CallParamsChange();
            }
        }


        private bool _IsServer = true;

        public bool IsServer
        {
            get { return _IsServer; }
            set { _IsServer = value; }
        }

        private bool _Enabled = true;
        public bool Enabled
        {
            get
            {
                return _Enabled ;
            }
            set
            {
                _Enabled = value;
            }
        }

        public int SkinPageSize 
        {
            get
            {
                return PageSize;
            }
            set
            {
                if (PageSize == 0)
                    PageSize = value;
            }
        }


        public int MaxPageIndex
        {
            get
            {
                int nPage = (TotalCount / PageSize);
                if (TotalCount % PageSize != 0)
                {
                    nPage++;
                }

                return nPage - 1;
            }
        }
    }
}
