﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using Dims.Web.Common;

namespace Dims.Web.Controls
{
    
    public class UserMessagesList : ControlWithPresenter
    {
        protected override void OnInit(EventArgs e)
        {
            
            Presenter.InitMessageView(this);
            base.OnInit(e);
        }

        public string MessageClass { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            foreach(UserMessage msg in  Presenter.MessagesList )
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.InnerText = msg.Message ;
                div.Attributes["class"] = MessageClass;

                this.Controls.Add(div);
            }
        }
    }
}
