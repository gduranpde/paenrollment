﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Collections;
using Dims.Web.Common;

namespace Dims.Web.Controls
{
    public class RepeaterEx:System.Web.UI.WebControls.Repeater , IListView
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Presenter.InitView(this);

            this.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(RepeaterEx_ItemDataBound);
        }

        void RepeaterEx_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            
        }
        private IPresenter _Presenter;
        public IPresenter Presenter
        {
            get
            {
                if (_Presenter == null && NamingContainer != null)
                {

                    _Presenter = (IPresenter)this.NamingContainer.FindControl(PresenterID); //(IPresenter) this.GetDataSource(); 
                }
                return _Presenter;
            }
        }

        [IDReferenceProperty(typeof(IPresenter))]
        public string PresenterID { get; set; }

        void IListView.DataBind()
        {
            var data = Presenter.Select() as IList;
            this.DataSource = data;
            base.DataBind();

            if(this.Visible)
                this.Visible = data.Count > 0;

        }
        protected override void OnItemCommand(System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            Presenter.OnCommand(e.CommandName, e.CommandArgument);

            base.OnItemCommand(e);
        }

        public List<T> GetSelectedItems<T>(string cbID, string keyID)
        {

            List<T> ids = new List<T>();
            foreach (System.Web.UI.WebControls.RepeaterItem item in Items)
            {
                var checkBox = item.FindControl(cbID) as ICheckBoxControl;

                if (checkBox.Checked)
                {
                    var h = item.FindControl(keyID) as System.Web.UI.WebControls.HiddenField;
                    ids.Add(DAL.Convertor.Cast<T>(h.Value) );
                }
            }

            return ids;

            
        }

        #region IListView Members


        public void ShowColumn(string columnName, bool show)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
