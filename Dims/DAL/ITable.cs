﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dims.DAL
{
    public interface ITable<TEntity>
    {
        List<TEntity> GetManyPaged(IPager pager, params object[] Filter);
        List<TEntity> GetMany(params object[] Filter);
        List<TEntity> GetAllPaged(IPager pager, params object[] Filter);
        List<TEntity> GetAll(params object[] Filter);
        TEntity GetOne(params object[] Filter);
        void Add(TEntity obj);
        void Save(TEntity obj);
        void Remove(params object[] obj);
        DAL.DataBase db { get; }
    }

    public interface ITabler<TEntity>
    {
        ITable<TEntity> GetTable();
    }
}
