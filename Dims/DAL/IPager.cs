﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dims.DAL
{
    public interface IPager
    {

        int PageIndex { get; set; }
        int PageSize { get; set; }
        int TotalCount { get; set; }
        bool IsServer { get; }
        bool Enabled { set; get; }

    }
}
