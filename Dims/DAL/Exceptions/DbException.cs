using System;
using System.Collections.Generic;
using System.Text;

namespace Dims.DAL
{
    public enum DbExceptionType:int {InitTables,CreateProvider,OpenConnection,ExecuteReader,ExecuteNonQuery,UserType,LoadData,MapParams };
    public class DbException:Exception
    {
        public static IDbExceptionMessenger ExceptionMessenger = new DbExceptionMessengerDefault();
        public static bool ShowInnerException = true;

        int _code;
        public DbException(DbExceptionType type)
        {
            _code = (int) type;
            
        }
        public DbException(int code)
        {
            _code = code;

        }

        public DbException(DbExceptionType type,Exception exp):base("db exception",exp)
        {
            _code = (int)type;
           

        }
        public DbException(int type, Exception exp)
            : base("db exception", exp)
        {
            _code = type;
            

        }


        public override string Message
        {
            get
            {
                string mess = "unknown db exception";
                try
                {
                    if (ExceptionMessenger == null)
                    {
                        mess = "Exception code " + _code;
                    }
                    else
                    {
                        mess = ExceptionMessenger.ParseException(_code);
                    }
                }
                catch
                {
                    
                }

                if (ShowInnerException && InnerException!=null)
                {
                    mess += "\n" +InnerException;
                }
                return mess;
            }
        }
    }
}
