using System;
using System.Collections.Generic;
using System.Text;

namespace Dims.DAL
{
    class DbExceptionMessengerDefault:IDbExceptionMessenger
    {
        

        public string ParseException(int code)
        {
            switch ((DbExceptionType)code)
            {
                case DbExceptionType.CreateProvider:
                    return "can`t create db provider";

                case DbExceptionType.OpenConnection:
                    return "can`t open connection";

                case DbExceptionType.InitTables:
                    return "can`t init tables";

                case DbExceptionType.ExecuteReader:
                    return "can`t execute reader";

                default: return "unknown db error";
            }
        }

        
    }
}
