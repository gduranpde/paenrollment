using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;



namespace Dims.DAL
{
    public class Convertor
    {
        public static string Hash(string s)
        {
            string ret = null;
            try
            {
                MD5 md = MD5.Create();

                ret = Convert.ToBase64String(md.ComputeHash(Encoding.Unicode.GetBytes(s.ToCharArray())));
            }
            catch{}

            
            return ret;
        }

        public static string NewHashKey
        {
            get
            {
                byte[] data = new byte[0x10];
                new RNGCryptoServiceProvider().GetBytes(data);
                return Convert.ToBase64String(data);

            }
        }

        public static string Hash(string s,string salt)
        {
            string ret = null;
            byte[] bytes = Encoding.Unicode.GetBytes(s);
            byte[] src = Convert.FromBase64String(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            byte[] inArray = null;
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);

            HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
               
            inArray = algorithm.ComputeHash(dst);
            
            ret =  Convert.ToBase64String(inArray);

            return ret;

        }

        


        public static Guid ToGuid(string data)
        {
            try
            {
                return new Guid(data);
            }
            catch
            {
                return Guid.Empty;
            }
        }

        public static string ToString(object value)
        {
            return string.Format("{0}", value);
        }
        public static string ToNullString(object value)
        {
            string str =  string.Format("{0}", value);

            if (string.IsNullOrEmpty(str))
                return null;

            return str;
        }
        public static Guid? ToNullGuid(string p , bool replaceEmptyByNull)
        {
            try
            {
                var ret = new Guid(p);

                if ( replaceEmptyByNull && ret == Guid.Empty )
                    return null;
                return ret;
            }
            catch
            {
                return null; 
            }
        }

        public static Guid? ToNullGuid(string p)
        {
            return ToNullGuid(p, true);
        }

        public static Guid? ToNullGuid(object p)
        {
            try
            {
                var ret = new Guid(p.ToString());

                return ret;
            }
            catch
            {
                return null;
            }
        }

        public static int ToInt(string data, int ret)
        {
            try
            {
                return Convert.ToInt32(data);
            }
            catch
            {
                return ret;
            }
        }

        public static int ToInt(object data)
        {
            try
            {
                return Convert.ToInt32(data);
            }
            catch
            {
                return 0;
            }
        }

        public static bool? ToNullBoolean(string p)
        {
            try
            {
                return Convert.ToBoolean(p);
            }
            catch
            {
                return null;
            }
        }

        public static float ToFloatZero(string p)
        {
            try
            {
                return float.Parse(p);
            }
            catch
            {
                return 0;
            }
        }



        public static short ToShortZero(string p)
        {
            try
            {
                return short.Parse(p);
            }
            catch
            {
                return 0;
            }
        }



        public static bool ToBool(object p)
        {
            try{
                return Convert.ToBoolean(p);
            }catch{}

            return false;
        }

        public static decimal ToDecimal(string p)
        {
       
            try
            {
                return decimal.Parse(p);
            }
            catch
            {
                return 0;
            }
        
        }


        public static Guid ToGuid(object obj)
        {
            return ToGuid(string.Format("{0}",obj));
        }

        public static DateTime ToDate(object value)
        {
            try
            {
                return Convert.ToDateTime(value);
            }
            catch{}

            return DateTime.MinValue;
        }

        public static int? ToNullInt(object p)
        {
            try
            {
                return Convert.ToInt32(p);
            }
            catch { }

            return null;
        }

        public static decimal? ToNullDecimal(object p)
        {
            try
            {
                return Convert.ToDecimal(p);
            }
            catch { }

            return null;
        }

        public static T Cast<T>(object p)
        {
            try
            {
                if (typeof(T).Equals(typeof(Guid)))
                    return (T)(object)Convertor.ToGuid(p);

                return (T)Convert.ChangeType(p, typeof(T));

            }catch{}

            return default(T);
        }
    }
}
