﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dims.DAL
{
    public class PagerEntity:IPager
    {
        public PagerEntity()
        {
            PageSize = 20;
            Enabled = true;
        }

        #region IPager Members

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public int TotalCount
        {
            get;
            set;
        }

        public bool IsServer
        {
            get { return false; } 
         
        }


        public bool Enabled
        { get; set; }
        #endregion
    }
}
