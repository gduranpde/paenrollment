﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Shared/Default.Master" AutoEventWireup="true"
    CodeBehind="MasterCustomerDatabaseUpdate.aspx.cs" 
    Inherits="PAEnrollment.MasterCustomerDatabaseUpdate" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>Upload New Master Customer Database</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;Please select a file to upload.
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>The file must be in CSV format with a header row and 6 columns:
                                <br />
                                Invitation Code, Account Number (Old), Mailing ZIP, Service ZIP,
                                <br />
                                Account Number (Main) and Operating Company.
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Text="File Name :"></asp:Label>
                                        </td>
                                        <td style="width: 20px"></td>
                                        <td>
                                            <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None"
                                                Height="24px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:RadioButtonList runat="server" ID="RadioButtonList" AutoPostBack="false">
                                                            <asp:ListItem Value="Replace">Replace</asp:ListItem>
                                                            <asp:ListItem Value="Append">Append</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="Button1" runat="server" Text="Upload" OnClick="UploadButton" />
                                                    </td>
                                                    <td style="width: 15px"></td>
                                                    <td>
                                                        <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="CancelButton" />
                                                    </td>
                                                     <td style="width: 15px"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>
