﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Shared/Default.Master" AutoEventWireup="true"
    CodeBehind="CustomErrorPage.aspx.cs" Inherits="PAEnrollment.CustomErrorPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <p>
       <%-- “We are sorry, but there has been an error on the website. Please call (888) 287-7304
        to speak with a representative.”--%>
        “Please call us at 1-888-225-8996 Monday - Friday between the hours of 9am - 7pm EST to speak to a live agent to complete your request 
         for your Energy Conservation Kit.”
      </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>