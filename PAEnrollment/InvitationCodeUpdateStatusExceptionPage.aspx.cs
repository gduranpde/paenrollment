﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PAEnrollment
{
    public partial class InvitationCodeUpdateStatusExceptionPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Status = Convert.ToString(Request.QueryString["Status"]);
            lblRecordCount.Text = Convert.ToString(Request.QueryString["No"]);
            lblExceptionCount.Text = Convert.ToString(Request.QueryString["ErrNo"]);

            if (Status == "S")
            {
                lblUploadStatus.Text = " Successfully.";
            }
            else
            {
                lblUploadStatus.Text = " Failed.";
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Admin/ExceptionListDatabaseUpdateHistoryPage.aspx");
        }
    }
}