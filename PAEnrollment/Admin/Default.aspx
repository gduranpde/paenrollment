﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CFLKits.Web.Admin.Default"MasterPageFile="~/App_Shared/Default.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="Content">
	<asp:Login ID="Login1" runat="server" OnLoggedIn="OnLoggedIn" OnLoggingIn="OnLoggingIn"
		DisplayRememberMe="true" RememberMeSet="true">
		<LayoutTemplate>
			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
				<tr>
					<td>
						<table border="0" cellpadding="3">
							<tr>
								<td class="label">
									<asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
								</td>
								<td>
									<asp:TextBox ID="UserName" runat="server" Width="200"></asp:TextBox>
									<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
										ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="label">
									<asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
								</td>
								<td>
									<asp:TextBox ID="Password" runat="server" TextMode="Password" Width="200"></asp:TextBox>
									<asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
										ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
									<asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." />
								</td>
							</tr>
							<tr>
								<td align="center" colspan="2" style="color: Red;">
									<asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
								</td>
							</tr>
							<tr>
								<td align="center" colspan="2" style="color: Red;">
									<asp:Literal ID="InstructionText" runat="server" EnableViewState="False"></asp:Literal>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td class="commandButton">
									<asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="Login1" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</LayoutTemplate>
	</asp:Login>
</asp:Content>
