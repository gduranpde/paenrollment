﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CFLKits.Data.Filters;
using CFLKits.Data.Presenters;
using Telerik.Web.UI;

namespace PAEnrollment.Admin
{
    public partial class ExceptionListDatabaseUpdateHistoryPage : System.Web.UI.Page
    {
        EnrollmentsPresenter presenter = new EnrollmentsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CFLKitsDb"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var cmd = conn.CreateCommand();
                conn.Open();
                cmd.CommandText = "select count(AccountNumber) from Exception";
                var res = cmd.ExecuteScalar();
                lblRecordsNo.InnerText = "Current Records in Exception Database: " + res.ToString();
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ExceptionListDatabaseUpdate.aspx");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("EnrollmentManagement.aspx");
        }

        protected void RadGrid_Enrollments_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CFLKitsDb"].ToString();
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            conn.Open();
            string selectQuery = "SELECT * from ExceptionDatabaseUploadHistory";
            adapter.SelectCommand = new SqlCommand(selectQuery, conn);
            adapter.Fill(dt);
            RadGrid_Enrollments.DataSource = dt;
            conn.Close();
        }
    }
}