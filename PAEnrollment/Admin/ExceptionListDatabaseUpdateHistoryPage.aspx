﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExceptionListDatabaseUpdateHistoryPage.aspx.cs"
    Inherits="PAEnrollment.Admin.ExceptionListDatabaseUpdateHistoryPage" MasterPageFile="~/App_Shared/Default.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Content">
    <div>
        <h1>Exception Database Update History</h1>
    </div>
    <table>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td style="vertical-align: bottom" align="left">
                            <asp:Button ID="btnBack" runat="server" Text="<< Back"
                                OnClick="btnBack_Click" />
                        </td>

                        <td style="text-align:center">
                          <label id="lblRecordsNo" runat="server"></label>
                        </td>

                        <td style="vertical-align: bottom" align="right">
                            <asp:Button ID="btnUpload" runat="server" Text="Upload New File"
                                OnClick="btnUpload_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="RadGrid_Enrollments" runat="server" Width="910" GridLines="None"
                    AutoGenerateColumns="false" PageSize="30" AllowSorting="True" AllowPaging="True"
                    Skin="Windows7" OnNeedDataSource="RadGrid_Enrollments_NeedDataSource">
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true">
                    </ExportSettings>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                        <Resizing AllowColumnResize="true" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="UploadFileId" AllowMultiColumnSorting="True" Width="890" 
                        CommandItemDisplay="Top" AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true"
                        PagerStyle-Mode="NextPrevNumericAndAdvanced">
                        <SortExpressions>
                          <telerik:GridSortExpression FieldName="UploadFileId" SortOrder="Descending" />
                        </SortExpressions>
                        <CommandItemTemplate>
                            <table class="rcCommandTable" width="100%">
                                <td align="left">
                                    <asp:Button ID="Button1" runat="server" Text=" " CssClass="rgExpXLS" CommandName="ExportToExcel" Title="Export to Excel" Style="margin-right: 2px; margin-left: 2px; margin-top: 2px; margin-bottom: 2px;" />
                                    <asp:Button ID="Button2" runat="server" Text=" " CssClass="rgExpPDF" CommandName="ExportToPdf" Title="Export to PDF" Style="margin-right: 2px; margin-left: 2px; margin-top: 2px; margin-bottom: 2px;" />
                                    <asp:Button ID="Button3" runat="server" Text=" " CssClass="rgExpCSV" CommandName="ExportToCSV" Title="Export to CSV" Style="margin-right: 2px; margin-left: 2px; margin-top: 2px; margin-bottom: 2px;" />
                                    <asp:Button ID="Button4" runat="server" Text=" " CssClass="rgExpDOC" CommandName="ExportToWord" Title="Export to Word" Style="margin-right: 2px; margin-left: 2px; margin-top: 2px; margin-bottom: 2px;" />
                                </td>
                                <td align="right">
                                    <asp:Button ID="Button5" runat="server" Text=" " Title="Refresh" CssClass="rgRefresh" CommandName="Refresh" Style="margin-right: 0px;" />
                                    <asp:LinkButton ID="linkButton1" runat="server" Text="Refresh" CommandName="Refresh" Style="vertical-align: top; margin-left: 0px;" />
                                </td>
                            </table>
                        </CommandItemTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="UploadFileId" HeaderText="Upload ID" SortExpression="UploadFileId"
                                UniqueName="UploadFileId" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false" ReadOnly="true" HeaderStyle-Width="90" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="UploadDate" HeaderText="Upload Date"
                                SortExpression="UploadDate" UniqueName="UploadDate" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="130" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="FileName" HeaderText="File Name" SortExpression="FileName"
                                UniqueName="FileName" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false" HeaderStyle-Width="250">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ReplaceOrAppend" HeaderText="Replace/Append" SortExpression="ReplaceAppend"
                                UniqueName="ReplaceAppend" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false" HeaderStyle-Width="140" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RecordsUploaded" HeaderText="Records Uploaded"
                                SortExpression="RecordsUploaded" UniqueName="RecordsUploaded" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false" HeaderStyle-Width="140" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Exceptions" HeaderText="Exceptions"
                                SortExpression="Exceptions" UniqueName="Exceptions" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                            </telerik:GridBoundColumn>
                        </Columns>

                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <script type="text/javascript">

        function Expand(itemID) {
            var Grid = $find('<%=RadGrid_Enrollments.ClientID %>');
               var scrollArea = document.getElementById('RadGrid_Enrollments')
               var rowElement = document.getElementById(itemID);
               window.scrollTo(0, rowElement.offsetTop + 200);
           }
    </script>
</asp:Content>
