﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationPage.aspx.cs" Inherits="CFLKits.Web.ConfirmationPage" MasterPageFile="~/App_Shared/Default.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-24848557-1']);
        _gaq.push(['_setDomainName', '.mycflkit.com']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <p>
        Thank you for your request. Your kit will arrive
         within the next 4-6 weeks. 
        If you have any questions, please call us at 1-888-527-7496.
    </p>

    <table>
        <%--<tr>
			<td colspan="2">
				<asp:Label runat="server" ID="lbGovernment"></asp:Label>
			</td>
		</tr>--%>
        <%--<tr>
			<td>
				<asp:HyperLink ID="hlEnrollmentPage" runat="server" Text="Enroll Another Property"
					NavigateUrl="~/EnrollmentRequest.aspx" />
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
				<a href="http://www.firstenergycorp.com/index.html">FirstEnergy WebSite</a>
			</td>
			<td>
			</td>
		</tr>--%>
        <br />
        <br />
        <tr>
            <td>Account #:
            </td>
            <td>
                <asp:Label runat="server" ID="lbAccountNumber"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Invitation Code:
            </td>
            <td>
                <asp:Label runat="server" ID="lbInvitationCode"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>First Name:
            </td>
            <td>
                <asp:Label runat="server" ID="lbContactFirstName"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Last Name:
            </td>
            <td>
                <asp:Label runat="server" ID="lbContactLastName"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>E-mail Address:
            </td>
            <td>
                <asp:Label runat="server" ID="lbContactEmail"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Phone Number:
            </td>
            <td>
                <asp:Label runat="server" ID="lbContactPhone"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>ZIP Code:
            </td>
            <td>
                <asp:Label runat="server" ID="lbContactZipCode"></asp:Label>
            </td>
        </tr>
         <tr>
            <td>Water Heater Fuel:
            </td>
            <td>
                <asp:Label runat="server" ID="lbWaterHeaterFuel"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Heater Fuel:
            </td>
            <td>
                <asp:Label runat="server" ID="lbHeaterFuel"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Referral Source:
            </td>
            <td>
                <asp:Label runat="server" ID="lbReferralSource"></asp:Label>
            </td>
        </tr>
       <tr>
            <td>More Info:
            </td>
            <td>
                <asp:Label runat="server" ID="labelMoreInfo"></asp:Label>
            </td>
        </tr>
       <%-- <tr>
            <td>Kit Selection:
            </td>
            <td>
                <asp:Label runat="server" ID="lbKit"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td>Shipping Address:
            </td>
            <td>
                <asp:Label runat="server" ID="lbShippingAddressType"></asp:Label>
            </td>
        </tr>
        <tr id="trAdr1" runat="server" visible="false">
            <td>Address Line 1:
            </td>
            <td>
                <asp:Label runat="server" ID="lbAddressLine1"></asp:Label>
            </td>
        </tr>
        <tr id="trAdr2" runat="server" visible="false">
            <td>Address Line 2:
            </td>
            <td>
                <asp:Label runat="server" ID="lbAddressLine2"></asp:Label>
            </td>
        </tr>
        <tr id="trCity" runat="server" visible="false">
            <td>City:
            </td>
            <td>
                <asp:Label runat="server" ID="lbCity"></asp:Label>
            </td>
        </tr>
        <tr id="trState" runat="server" visible="false">
            <td>State:
            </td>
            <td>PA
            </td>
        </tr>
        <tr id="trZip" runat="server" visible="false">
            <td>ZIP:
            </td>
            <td>
                <asp:Label runat="server" ID="lbZipOtherShipping"></asp:Label>
            </td>
        </tr>

    </table>

   <%-- <p>
        The cost of energy efficiency programs in Maryland, including this kit, is funded through customer rates in accordance with EmPower Maryland Act of 2008. 
        <i>FirstEnergy does not provide a warranty or endorse any manufacturer or product. You will not be charged any additional fees</i>
    </p>--%>
    <br />
    <p style="text-align: center"><a href="http://www.firstenergycorp.com/index.html">FirstEnergy Website</a></p>


    <!-- Google Code for Mycfl signup Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1032368160;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "yFz7COLCmQIQoOCi7AM";
        var google_conversion_value = 0;
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1032368160/?label=yFz7COLCmQIQoOCi7AM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
</asp:Content>
