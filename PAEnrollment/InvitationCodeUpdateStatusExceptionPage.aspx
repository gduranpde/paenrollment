﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Shared/Default.Master" AutoEventWireup="true"
    CodeBehind="InvitationCodeUpdateStatusExceptionPage.aspx.cs" Inherits="PAEnrollment.InvitationCodeUpdateStatusExceptionPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp; Exception Table Uploaded
                <asp:Label ID="lblUploadStatus" runat="server" Text=""></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp; Number of Records Uploaded :
                <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp; Number of Exceptions :
                <asp:Label ID="lblExceptionCount" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-left: 70px">
                            <asp:Button ID="btnBack" runat="server" Text="BACK" Width="100px" OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>