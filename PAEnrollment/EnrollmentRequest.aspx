﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnrollmentRequest.aspx.cs" Inherits="CFLKits.Web.EnrollmentRequest" MasterPageFile="~/App_Shared/Default.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var invCodeOraccountRequired = function (sender, args) {
            var invitation = document.getElementById('<%= txtInvitationCode.ClientID %>');
            var account = document.getElementById('<%= txtAccountNo.ClientID %>');
            args.IsValid = true;
            if (invitation.value == '' && account.value == '') {
                args.IsValid = false;
            }
        }
        var firstLastNameRequired = function (sender, args) {
            var lastName = document.getElementById('<%= txtLastName.ClientID %>');
            var firstName = document.getElementById('<%= txtFirstName.ClientID %>');

            args.IsValid = true;

            if (lastName.value == '' || firstName.value == '') {
                args.IsValid = false;
            }
        }
        var phoneRequired = function (sender, args) {
            var phone1 = document.getElementById('<%= txtPhoneNo1.ClientID %>');
            var phone2 = document.getElementById('<%= txtPhoneNo2.ClientID %>');
            var phone3 = document.getElementById('<%= txtPhoneNo3.ClientID %>');

            args.IsValid = true;

            if (phone1.value == '' || phone2.value == '' || phone3.value == '') {
                args.IsValid = false;
            }
        }
        var phoneFormat = function (sender, args) {
            var phone1 = document.getElementById('<%= txtPhoneNo1.ClientID %>');
            var phone2 = document.getElementById('<%= txtPhoneNo2.ClientID %>');
            var phone3 = document.getElementById('<%= txtPhoneNo3.ClientID %>');

            args.IsValid = true;

            if (!(phone1.value == '' || phone2.value == '' || phone3.value == '') && !(phone1.value + phone2.value + phone3.value).match(/^\d{10}$/)) {
                args.IsValid = false;
            }
        }

        var customerClassRequired = function (sender, args) {
            var checkboxes = document.getElementById('wrapperCustomerClass').getElementsByTagName('input');
            args.IsValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    args.IsValid = true;
                    break;
                }
            }
        }

        var bothEmailsFilled = function (sender, args) {
            var email = document.getElementById('<%= txtEmail.ClientID %>');
            var confirmEmail = document.getElementById('<%= txtConfirmEmail.ClientID %>');

            if (email.value != '' && confirmEmail.value == '') {
                args.IsValid = false;
            }
        }

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-24848557-1']);
        _gaq.push(['_setDomainName', '.mycflkit.com']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

        function ShowError(error) {
            $("#errorMsg").append('<span>Unable to Complete Request</span>');
            $("#errorPanel").dialog({
                maxHeight: 230,
                maxWidth: 300,
                minHeight: 230,
                minWidth: 300,
                modal: true
            });
        }

        function CloseModal() {
            $("#errorPanelDuplicate").dialog('close')
        };

        function ShowErrorDuplicate(error) {
            $("#errorMessageDuplicate").append('<span>Unable to Complete Request</span>');
            $("#errorPanelDuplicate").dialog({
                maxHeight: 250,
                maxWidth: 300,
                minHeight: 250,
                minWidth: 300,
                modal: true
            });
        }
        $(function () {
            $("#btnShowMoreInfo").click(function () {
                $("#dialog").dialog({
                    maxHeight: 250,
                    maxWidth: 500,
                    minHeight: 250,
                    minWidth: 500,
                    modal: true
                });
            });

            $("#ctl00_Content_btnSubmit").click(function () {
                $("#errorDiv").html('');
                var invCodeOraccountRequired = true;
                var invitation = document.getElementById('<%= txtInvitationCode.ClientID %>');
                var account = document.getElementById('<%= txtAccountNo.ClientID %>');
                if (invitation.value == '' && account.value == '') {
                    invCodeOraccountRequired = false;
                    $("#errorDiv").append('<span>Please enter Invitation Code OR Account #</span>');
                    $("#errorDiv").append('</br>');
                }

                var firstLastNameRequired = true;
                var lastName = document.getElementById('<%= txtLastName.ClientID %>');
                var firstName = document.getElementById('<%= txtFirstName.ClientID %>');

                if (lastName.value == '' || firstName.value == '') {
                    firstLastNameRequired = false;
                    $("#errorDiv").append('<span>Please enter your First and Last Name</span>');
                    $("#errorDiv").append('</br>');
                }

                var phoneRequired = true;
                var phoneFormat = true;
                var phone1 = document.getElementById('<%= txtPhoneNo1.ClientID %>');
                var phone2 = document.getElementById('<%= txtPhoneNo2.ClientID %>');
                var phone3 = document.getElementById('<%= txtPhoneNo3.ClientID %>');

                if (phone1.value == '' || phone2.value == '' || phone3.value == '') {
                    phoneRequired = false;
                    $("#errorDiv").append('<span>Please enter phone number</span>');
                    $("#errorDiv").append('</br>');
                }

                if (!(phone1.value == '' || phone2.value == '' || phone3.value == '') && !(phone1.value + phone2.value + phone3.value).match(/^\d{10}$/)) {
                    phoneFormat = false;
                    $("#errorDiv").append('<span>Phone must be in format ###-###-####</span>');
                    $("#errorDiv").append('</br>');
                }

                var zipOk = true;
                var zip = document.getElementById('<%= txtZip.ClientID %>');
                if (zip.value == '') {
                    zipOk = false;
                    $("#errorDiv").append('<span>Please enter ZIP code</span>');
                    $("#errorDiv").append('</br>');
                }
                else
                    if (zip.value.length < 5) {
                        zipOk = false;
                        $("#errorDiv").append('<span>ZIP code must be 5 digits long</span>');
                        $("#errorDiv").append('</br>');
                    }

                var ddlWater = true;
                var waterHeaterFuel = document.getElementById('<%= ddlWaterHeaterFuel.ClientID %>');
                if (waterHeaterFuel.value == '') {
                    ddlWater = false;
                    $("#errorDiv").append('<span>Please select Water Heating Fuel</span>');
                    $("#errorDiv").append('</br>');
                }

                var ddlHeater = true;
                var heaterFuel = document.getElementById('<%= ddlHeaterFuel.ClientID %>');
                if (heaterFuel.value == '') {
                    ddlHeater = false;
                    $("#errorDiv").append('<span>Please select Heating Fuel</span>');
                    $("#errorDiv").append('</br>');
                }

                var referal = true;
                var referralSource = document.getElementById('<%= ddlReferralSources.ClientID %>');
                if (referralSource.value == '') {
                    referal = false;
                    $("#errorDiv").append('<span>Please select Referral Source</span>');
                    $("#errorDiv").append('</br>');
                }

                if (!firstLastNameRequired ||
                  !invCodeOraccountRequired ||
                  !phoneRequired ||
                  !phoneFormat ||
                  !zipOk || !ddlHeater || !ddlWater || !referal) {
                    $("#errorDialog").dialog({
                        maxHeight: 230,
                        maxWidth: 300,
                        minHeight: 230,
                        minWidth: 300,
                        modal: true
                    });
                }
            });
        });

    </script>
</asp:Content>
<asp:content contentplaceholderid="Content" runat="server">
  <asp:ValidationSummary runat="server" DisplayMode="BulletList" ShowMessageBox="false"
    ShowSummary="false" ID="valSum" />
  <div>
    <h1>FirstEnergy Energy Conservation Kit Program</h1>
    <p class="disclamer">
      <strong>FirstEnergy has contracted with Power Direct Energy to administer this program. 
		  Power Direct Energy maintains this site and its content. 
		  To request a kit over the phone, call 1-888-304-1623. For assistance with signing up for a kit online,
		   please contact a Power Direct Energy program representative at 1-844-517-9229, Monday-Friday between the hours of 9am-7pm EST. 
      </strong>
    </p>
    <p>
     Pennsylvania residential customers of Met-Ed, Penelec, Penn Power, and West Penn Power are now eligible to receive an energy conservation kit. <strong>There is no additional cost for this kit.</strong>
     The kit includes seven compact fluorescent light bulbs (CFLs) in various wattages, a smart strip/surge protector, a furnace filter whistle and two LED night lights.**
          <a id="btnShowMoreInfo" href="#">Click here to learn more about the cost of this kit.</a>
    </p>
    <p>
     To receive your kit, please complete the fields below. If you received a postcard, you can use the seven digit invitation code listed above your name to enroll. <b>If you do not have an invitation code, you will need to provide your 12 digit account number</b>,
     which can be found on the upper right hand corner of your electric bill. 
    </p>
    <table cellpadding="3" cellspacing="0">
      <tr>
        <td class="label">Invitation Code: *
        </td>
        <td class="data" style="white-space: nowrap; width: 100%;">
          <table cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <asp:TextBox runat="server" ID="txtInvitationCode" Width="100" MaxLength="256"></asp:TextBox>
              </td>
              <td class="label" style="width: 100px; padding-top: 2px;"><b>or</b> Account #: *
              </td>
              <td class="data">
                <asp:TextBox runat="server" ID="txtAccountNo" Width="140" MaxLength="15"></asp:TextBox>
              </td>
              <td class="validator">
                <asp:CustomValidator runat="server" ClientValidationFunction="invCodeOraccountRequired"
                  ErrorMessage="Please enter Invitation Code OR Account #">*</asp:CustomValidator>
              </td>
              <td class="label">(do not include any dashes or spaces)</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="label">Contact Name: *
        </td>
        <td>
          <table cellpadding="0" cellspacing="0">
            <tr>
              <td class="data" style="padding-right: 20px;">
                <asp:TextBox runat="server" ID="txtFirstName" Width="165"></asp:TextBox>
              </td>
              <td class="data">
                <asp:TextBox runat="server" ID="txtLastName" Width="165"></asp:TextBox>
              </td>
              <td class="validator">
                <asp:CustomValidator runat="server" ClientValidationFunction="firstLastNameRequired"
                  ErrorMessage="Please enter your First and Last Name">*</asp:CustomValidator>
              </td>
            </tr>
            <tr>
              <td>First
              </td>
              <td>Last
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="label">Email Address:
        </td>
        <td class="data">
          <asp:TextBox runat="server" ID="txtEmail" Width="200" MaxLength="2048" OnTextChanged="Email_OnChange" AutoPostBack="true"></asp:TextBox>
          <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" Display="Dynamic"
						ErrorMessage="Please enter your email">*</asp:RequiredFieldValidator>--%>
          <asp:RegularExpressionValidator runat="server" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            ErrorMessage="Wrong email format" EnableClientScript="true">Wrong email format</asp:RegularExpressionValidator>
        </td>
      </tr>
      <tr>
        <td class="label">Confirm Email:
        </td>
        <td class="data">
          <asp:TextBox runat="server" ID="txtConfirmEmail" Width="200" MaxLength="2048"></asp:TextBox>
          <%--  <asp:RequiredFieldValidator runat="server" ControlToValidate="txtConfirmEmail" Display="Dynamic"
						ErrorMessage="Please confirm email">*</asp:RequiredFieldValidator>--%>
          <asp:CompareValidator runat="server" ControlToValidate="txtConfirmEmail" ControlToCompare="txtEmail"
            Type="String" Operator="Equal" Display="Dynamic" ErrorMessage="Emails do not match">Emails do not match</asp:CompareValidator>
          <asp:CustomValidator runat="server" ClientValidationFunction="bothEmailsFilled" Display="Dynamic"
            ErrorMessage="Please confirm email">Please confirm email</asp:CustomValidator>
        </td>
        <td class="validator"></td>
      </tr>
      <tr>
        <td class="label">Phone Number: *
        </td>
        <td>
          <table cellpadding="0" cellspacing="0">
            <tr>
              <td class="data">
                <asp:TextBox runat="server" ID="txtPhoneNo1" Width="50" MaxLength="3"></asp:TextBox>
              </td>
              <td style="width: 20px; text-align: center">-
              </td>
              <td class="data">
                <asp:TextBox runat="server" ID="txtPhoneNo2" Width="50" MaxLength="3"></asp:TextBox>
              </td>
              <td style="width: 20px; text-align: center">-
              </td>
              <td class="data">
                <asp:TextBox runat="server" ID="txtPhoneNo3" Width="50" MaxLength="4"></asp:TextBox>
                &nbsp;
								<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="phoneRequired"
                  Display="Dynamic" ErrorMessage="Please enter phone number">*</asp:CustomValidator>
                <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="phoneFormat"
                  Display="Dynamic" ErrorMessage="Phone must be in format ###-###-####">*</asp:CustomValidator>
              </td>
            </tr>
            <tr>
              <td>###
              </td>
              <td></td>
              <td>###
              </td>
              <td></td>
              <td>####
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="label">ZIP Code: *
        </td>
        <td class="data">
          <asp:TextBox runat="server" ID="txtZip" Width="40" MaxLength="5"></asp:TextBox>
          <asp:RequiredFieldValidator runat="server" ControlToValidate="txtZip" Display="Dynamic"
            ErrorMessage="Please enter ZIP code">*</asp:RequiredFieldValidator>
          <asp:RegularExpressionValidator runat="server" ControlToValidate="txtZip" ValidationExpression="\d{5}"
            Display="Dynamic" ErrorMessage="ZIP code must be 5 digits long">*</asp:RegularExpressionValidator>
        </td>
        <td class="validator"></td>
      </tr>
      <tr>
        <td class="label" style="padding-top: 9px">Water Heating Fuel: *                      
        </td>
        <td>
          <table>
            <tr>
              <td class="data" style="vertical-align: top; padding: 4px 5px 0 0;">
                <asp:DropDownList ID="ddlWaterHeaterFuel" runat="server" AutoPostBack="true"
                  OnSelectedIndexChanged="ddlWaterHeaterFuel_SelectedIndexChanged">
                  <asp:ListItem Value="" Selected="True">--- Select ---</asp:ListItem>
                  <asp:ListItem Value="Electric">Electric</asp:ListItem>
                  <asp:ListItem Value="Non-electric">Non-electric</asp:ListItem>
                </asp:DropDownList>
              </td>
              <td style="width: 240px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorWaterHeaterFuel" runat="server"
                  ControlToValidate="ddlWaterHeaterFuel" Display="Static" ErrorMessage="Please select Water Heating Fuel">*</asp:RequiredFieldValidator>
                <asp:Label ID="Label1" runat="server">Non-electric water heating fuel includes natural gas, oil, propane, wood and other.</asp:Label>
              </td>

              <td class="label" style="padding-top: 3px; margin-left: 35px; width: 120px">Heating Fuel: *</td>
              <td class="data" style="vertical-align: top; padding: 4px 5px 0 0;">
                <asp:DropDownList ID="ddlHeaterFuel" runat="server" AutoPostBack="true"
                  OnSelectedIndexChanged="ddlHeaterFuel_SelectedIndexChanged">
                  <asp:ListItem Value="" Selected="True">--- Select ---</asp:ListItem>
                  <asp:ListItem Value="Electric">Electric</asp:ListItem>
                  <asp:ListItem Value="Non-electric">Non-electric</asp:ListItem>
                </asp:DropDownList>
              </td>
              <td style="width: 240px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                  ControlToValidate="ddlHeaterFuel" Display="Static" ErrorMessage="Please select Heating Fuel">*</asp:RequiredFieldValidator>
                <asp:Label ID="Label2" runat="server">Non-electric heating fuel includes natural gas, oil, propane, wood and other.</asp:Label>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="label">Referral Source: *</td>
        <td class="data">
          <table cellpadding="0" cellspacing="0">
            <tr>
              <td class="data" style="vertical-align: top; padding: 2px 5px 0 0;">
                <asp:DropDownList ID="ddlReferralSources" runat="server">
                  <asp:ListItem Value="">--- None ---</asp:ListItem>
                  <asp:ListItem Value="Direct mail">Direct mail</asp:ListItem>
                  <asp:ListItem Value="Bill Insert">Bill Insert</asp:ListItem>
                  <asp:ListItem Value="Press Release">Press Release</asp:ListItem>
                  <asp:ListItem Value="Friend">Friend/Family</asp:ListItem>
                  <asp:ListItem Value="Email">Email</asp:ListItem>
                  <asp:ListItem Value="Newspaper">Newspaper</asp:ListItem>
                  <asp:ListItem Value="Other">Other</asp:ListItem>
                </asp:DropDownList>
              </td>
              <td style="vertical-align: top; padding-top: 3px;">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorReferralSources" runat="server"
                  ControlToValidate="ddlReferralSources" Display="static" ErrorMessage="Please select Referral Source">*</asp:RequiredFieldValidator>
              </td>
            </tr>
          </table>
        </td>
        <td class="validator"></td>
      </tr>
      <asp:Panel runat="server" ID="pnlMoreInfo">
        <tr>
          <td class="label">More Info: *
          </td>
          <td>Would you like to receive periodic e-mails with energy saving information and offers?
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <asp:RadioButtonList runat="server" ID="RadioButtonList2" AutoPostBack="false">
              <asp:ListItem Value="Y" Selected="True">Yes</asp:ListItem>
              <asp:ListItem Value="N">No</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
      </asp:Panel>
    </table>
    <asp:UpdatePanel runat="server" ID="updPanelWaterHeaterFuelElectric">
      <ContentTemplate>
        <asp:Panel runat="server" ID="pnlKit" Visible="false">
          <p>
            Customers with electric water heating are eligible to receive 
            additional energy conservation products including a low-flow showerhead, two faucet 
            aerators, and four aerator adaptors. Please choose below which kit you would like to receive.
          </p>
          <asp:DropDownList ID="ddlKit" runat="server">
            <asp:ListItem Value="Standard Kit with Water Products" Selected="True">Standard Kit with Water Products</asp:ListItem>
            <asp:ListItem Value="Standard Kit without Water Products">Standard Kit without Water Products</asp:ListItem>
          </asp:DropDownList>
        </asp:Panel>
      </ContentTemplate>
      <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlWaterHeaterFuel" />
      </Triggers>
    </asp:UpdatePanel>
    <h2>Shipping Information</h2>
    <p>
        The kit may be shipped to your mailing address (where your bill is sent), 
        service address (the location where you receive your electricity) or an 
        alternative address. Please select where you would like the kit to be shipped. 
        Note that the kit can only be shipped to addresses in the state of Pennsylvania.
    </p>
    <table cellpadding="0" cellspacing="0">
      <tr>
        <td style="width: 100px;"></td>
        <td id="shippingInformationListWrapper">
          <asp:RadioButtonList runat="server" ID="lstShippingOptions" AutoPostBack="true" OnSelectedIndexChanged="lstShippingOptions_SelectedIndexChanged">
            <asp:ListItem Value="M">Mailing Address</asp:ListItem>
            <asp:ListItem Value="S">Service Address</asp:ListItem>
            <asp:ListItem Value="O">Other</asp:ListItem>
          </asp:RadioButtonList>
          <script type="text/javascript">
              $(function () {
                  if ($('input:radio:checked', '#shippingInformationListWrapper').length == 0) {
                      $('input:radio', '#shippingInformationListWrapper').get(0).checked = true;
                  }
              });
          </script>
        </td>
      </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="updPanelOtherShippingAddress">
      <ContentTemplate>
        <asp:Panel runat="server" ID="pnlOtherShippingAddress" Visible="false">
          <table cellpadding="3" cellspacing="0">
            <tr>
              <td class="label">Address Line 1: *
              </td>
              <td>
                <asp:TextBox runat="server" ID="txtAddress1" MaxLength="256" Width="400"></asp:TextBox>&nbsp;
								<asp:RequiredFieldValidator runat="server" ControlToValidate="txtAddress1" ErrorMessage="Please enter your address">*</asp:RequiredFieldValidator>
              </td>
            </tr>
            <tr>
              <td class="label">Address Line 2:
              </td>
              <td>
                <asp:TextBox runat="server" ID="txtAddress2" MaxLength="256" Width="400"></asp:TextBox>
              </td>
            </tr>
            <tr>
              <td class="label">City: *
              </td>
              <td>
                <asp:TextBox runat="server" ID="txtCity" MaxLength="64"></asp:TextBox>&nbsp;
								<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCity"
                  ErrorMessage="Please enter city name">*</asp:RequiredFieldValidator>
              </td>
            </tr>
            <tr>
              <td class="label">State:
              </td>
              <td>
                <asp:TextBox runat="server" ID="txtState" Text="PA" Enabled="false"></asp:TextBox>
              </td>
            </tr>
            <tr>
              <td class="label">ZIP: *
              </td>
              <td>
                <asp:TextBox runat="server" ID="txtZipOtherShipping" MaxLength="5" Width="40"></asp:TextBox>&nbsp;
								<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtZipOtherShipping"
                  ErrorMessage="Please enter ZIP code" Display="Dynamic">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtZipOtherShipping"
                  ValidationExpression="\d{5}" Display="Dynamic" ErrorMessage="ZIP code must be 5 digits long">*</asp:RegularExpressionValidator>
              </td>
              </td>
            </tr>
          </table>
        </asp:Panel>
      </ContentTemplate>
      <Triggers>
        <asp:AsyncPostBackTrigger ControlID="lstShippingOptions" />
      </Triggers>
    </asp:UpdatePanel>
    <p>**One Energy Conservation Kit per residential account. </p>
    <asp:Button runat="server" ID="btnSubmit" Text="Submit Request" Style="float: right;"
      OnClick="btnSubmit_OnClick" />
    <asp:Button runat="server" ID="btnClear" Text="Clear Form" Style="float: right; margin-right: 20px;"
      OnClick="btnClear_OnClick" ValidationGroup="ClearForm" />
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>

  <%-- <asp:Panel ID="accountExceptions" runat="server" CssClass="modalPopup ui-dialog ui-widget ui-widget-content ui-corner-all ui-front" HorizontalAlign="Center" style="visibility:hidden">
            <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" >
                <span class="ui-dialog-title">Error</span>
            </div>
            <asp:Label runat="server" ID="lbError" Text="" ForeColor="Red"></asp:Label>
            <br />
            <asp:Label runat="server" ID="lbErrorComments"
                Text="Our records indicate that the account number you provided has 
                      received a kit in the past (sometime within the last three years). 
                      Unfortunately, only one kit is allowed per account.
                      However, we will keep your information on hand and contact you in 
                      the event that the rules of the program change and we can send you an additional kit. Press OK to continue."></asp:Label>
            <br />
            <br />
            <asp:Button runat="server" ID="btnOk" Text="Ok" />
        </asp:Panel>

       <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="accountExceptions"
            TargetControlID="Panel1" OkControlID="btnOk" BackgroundCssClass="modalBackground">
        </ajaxToolkit>--%>

    <div style="clear: both;">
    </div>

    <div id="errorPanel" title="Error" style="display:none">
      <div class="modal-body">
        <span id="errorMsg" style="color: red"></span>
        <br />
        <span>Please make sure you entered your account number <b>without</b> spaces. 
              If you’re still having trouble enrolling, please call us at 1-888-527-7496 Monday -
              Friday between the hours of 9am - 7pm EST to speak to a live agent to complete your request
              for your Energy Conservation Kit.
        </span>
      </div>
    </div>

           
    <div id="errorPanelDuplicate" title="Duplicate kit" style="display:none">
      <div class="modal-body">
        <span id="errorMessageDuplicate" style="color: red"></span>
        <br />
        <span>Our records indicate that the account number you provided has already been enrolled to receive a kit (since May 2010). Unfortunately, only one kit is allowed per account. If you have any questions, please contact the Conservation Kit Customer Service line at 888-527-7496.
        </span>
        <br />
          <br />
         <%-- <div style="padding-left:75px">
        <button name="btnDuplicateOK" title="OK" onclick="CloseModal()" style="width:60px;height:25px;" value="OK">OK</button>
              </div>--%>
      </div>
    </div>

    <div id="errorDialog" title="Error">
      <div class="modal-body" id="errorDiv"></div>
    </div>

    <div id="dialog" title="What is the cost of this kit?" style="display: none">
      <div class="modal-body">
           <b>What is the cost of this kit?</b>
          <br />
           You will not be charged separately if you receive this Energy Conservation Kit. 
           The cost of these kits, as with the costs of all other state mandated residential energy efficiency programs, 
           is paid for through a charge included in all residential customers' bills. Offering these kits is one part of
           our Pennsylvania Energy Efficiency Programs, which were approved by the Public Utilities Commission of Pennsylvania. 
           These programs are necessary to comply with state mandated energy efficiency requirements included in Pennsylvania’s energy law, Act 129 of 2008. 
      </div>
    </div>
  </div>  
</asp:content>
