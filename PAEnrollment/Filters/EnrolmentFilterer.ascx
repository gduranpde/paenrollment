﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EnrolmentFilterer.ascx.cs"Inherits="CFLKits.Web.Filters.EnrolmentFilterer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table>
	<tr>
		
		<td class="label">From:</td>
		<td>
			
			<telerik:RadDatePicker ID="RadDatePicker_StartDate" runat="server" Skin="Telerik"
				Width="170px">
				<Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
					ViewSelectorText="x">
				</Calendar>
				<DatePopupButton HoverImageUrl="" ImageUrl="" />
				<DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
				</DateInput>
			</telerik:RadDatePicker>
		</td>
		<td class="label">To:</td>
		<td>
			
			<telerik:RadDatePicker ID="RadDatePicker_EndDate" runat="server" Skin="Telerik" Width="170px">
				<Calendar Skin="Telerik" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
					ViewSelectorText="x">
				</Calendar>
				<DatePopupButton HoverImageUrl="" ImageUrl="" />
				<DateInput DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy">
				</DateInput>
			</telerik:RadDatePicker>
		</td>
	</tr>
	<tr>
		<td class="label">
			Account # :
		</td>
		<td>
			<asp:TextBox ID="txtAccount" runat="server"></asp:TextBox>
		</td>
		<td class="label">
			Invitation Code :
		</td>
		<td>
			<asp:TextBox ID="txtInvitationCode" runat="server"></asp:TextBox>
		</td>
	</tr>
     <tr>
		<td class="label">
			Account # New:
		</td>
		<td>
			<asp:TextBox ID="txtAccount12" runat="server"></asp:TextBox>
		</td>
		<td class="label">
			Email :
		</td>
		<td>
			<asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="label">
			First Name :
		</td>
		<td>
			<asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
		</td>
		<td class="label">
			Last Name :
		</td>
		<td>
			<asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
		</td>
	</tr>
</table>
<div style="width: 30%; float: left;">
	<asp:Button runat="server" ID="Button1" Text="Search" Width="80px" CssClass="ptButton"
				OnClick="Button_Filter_Click" />
	<asp:Button runat="server" ID="Button2" Text="Clear" Width="80px" CssClass="ptButton"
				OnClick="Button_Clear_Click" />
</div>
