﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.UI.WebControls;
using CFLKits.Data.Presenters;
using CFLKits.Entities;
using System.Configuration;
using System.Data.SqlClient;

namespace CFLKits.Web
{
    public partial class EnrollmentRequest : System.Web.UI.Page
    {
        EnrollmentsPresenter presenter = new EnrollmentsPresenter();

        #region Fill

        protected void Page_Init(object sender, EventArgs e)
        {
            //   accountExceptions.Visible = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                pnlMoreInfo.Visible = false;
            }
            // errorPanel.Visible = false;
        }

        #endregion Fill

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            //lbError.Text = "";

            Enrollment enrollment = new Enrollment();

            enrollment.SubmittedDate = DateTime.Now;

            if (txtAccountNo.Text.Length > 12)
            {
                enrollment.AccountNumber = txtAccountNo.Text;
                enrollment.AccountNumber12 = "";
                Session["AC"] = txtAccountNo.Text;
            }
            else
            {
                enrollment.AccountNumber12 = txtAccountNo.Text;
                enrollment.AccountNumber = "";

                if (txtAccountNo.Text.Trim().Length > 0)
                {
                    Session["NAC"] = txtAccountNo.Text;
                }
            }

            enrollment.InvitationCode = txtInvitationCode.Text;
            Session["ICode"] = txtInvitationCode.Text;

            enrollment.ContactEmail = txtEmail.Text;
            enrollment.ContactFirstName = txtFirstName.Text;
            enrollment.ContactLastName = txtLastName.Text;
            enrollment.ContactPhone = txtPhoneNo1.Text + txtPhoneNo2.Text + txtPhoneNo3.Text;
            enrollment.FormattedPhone = txtPhoneNo1.Text + "-" + txtPhoneNo2.Text + "-" + txtPhoneNo3.Text;
            enrollment.ContactZipCode = txtZip.Text;

            enrollment.ReferralSource = ddlReferralSources.SelectedValue;
            enrollment.WaterHeaterFuel = ddlWaterHeaterFuel.SelectedValue;
            enrollment.HeaterFuel = ddlHeaterFuel.SelectedValue;

            if (string.Equals(RadioButtonList2.SelectedValue, "Y"))
            {
                if (txtEmail.Text == "")
                {
                    enrollment.MoreInfo = null;
                }
                else
                {
                    enrollment.MoreInfo = true;
                }
            }
            else
            {
                enrollment.MoreInfo = false;
            }

            if (ddlWaterHeaterFuel.SelectedValue == "Electric")
            {
                pnlKit.Visible = true;
            }
            else
            {
                pnlKit.Visible = false;
            }

            if (pnlKit.Visible)
            {
                if (ddlKit.SelectedValue == "Standard Kit with Water Products")
                {
                    enrollment.KitSelection = "Electric Kit";
                }
                else
                {
                    enrollment.KitSelection = "Non-electric Kit";
                }
            }
            else
            {
                enrollment.KitSelection = "Non-electric Kit";
            }

            enrollment.ShipToAddress = lstShippingOptions.SelectedValue;
            if (enrollment.ShipToAddress == "O")
            {
                enrollment.Address1 = txtAddress1.Text;
                enrollment.Address2 = txtAddress2.Text;
                enrollment.City = txtCity.Text;
                enrollment.State = txtState.Text;
                enrollment.Zip = txtZipOtherShipping.Text;
            }

            var error = presenter.SaveEnrollment(enrollment, false);

            if (error == ErrorMesages.NoError)
            {
                CFLKits.Data.Filters.EnrollmentDetails a = new Data.Filters.EnrollmentDetails();
                a.ContactFirstName = txtFirstName.Text;
                a.ContactLastName = txtLastName.Text;
                a.ContactPhone = txtPhoneNo1.Text + txtPhoneNo2.Text + txtPhoneNo3.Text;
                a.ContactZipCode = txtZip.Text;
                a.ReferralSource = ddlReferralSources.SelectedValue;
                a.WaterHeaterFuel = ddlWaterHeaterFuel.SelectedValue;


                List<Enrollment> aqq = presenter.GetEnrollmentID(a);

                long EnrollID = aqq[0].EnrollmentID;

                PAEnrollment.Listener.Listener objLst = new PAEnrollment.Listener.Listener();
                if (bool.Parse(ConfigurationManager.AppSettings["SandBox"].ToString()) == true)
                {
                    objLst.Url = ConfigurationManager.AppSettings["SandBoxUrl"].ToString();
                }

                try
                {
                    //bool status = objLst.OhioEnergyKitEnrollmentRecordInsert(EnrollID, enrollment.SubmittedDate, enrollment.InvitationCode,
                    //    enrollment.AccountNumber, enrollment.ContactFirstName, enrollment.ContactLastName,
                    //    enrollment.ContactEmail, enrollment.ContactPhone, enrollment.ContactZipCode, enrollment.ShipToAddress,
                    //    enrollment.Address1, enrollment.Address2, enrollment.City, enrollment.State, enrollment.Zip, enrollment.ReferralSource,
                    //    enrollment.KitSelection, enrollment.WaterHeaterFuel, enrollment.AccountNumber12, enrollment.ReplicationStatus,
                    //    enrollment.HeaterFuel, enrollment.MoreInfo, enrollment.OperatingCompany);
                    bool status = objLst.PAEnergyEnrollmentRecordInsert(EnrollID, enrollment.SubmittedDate, enrollment.InvitationCode,
                      enrollment.AccountNumber, enrollment.ContactFirstName, enrollment.ContactLastName,
                      enrollment.ContactEmail, enrollment.ContactPhone, enrollment.ContactZipCode, enrollment.ShipToAddress,
                      enrollment.Address1, enrollment.Address2, enrollment.City, enrollment.State, enrollment.Zip, enrollment.ReferralSource,
                      enrollment.KitSelection, enrollment.WaterHeaterFuel, enrollment.AccountNumber12, enrollment.ReplicationStatus,
                      enrollment.HeaterFuel, enrollment.MoreInfo, enrollment.OperatingCompany);
                    if (status)
                    {
                        presenter.UpdateReplicationStatus(EnrollID);
                    }
                }
                catch
                {
                }

                Session["Enrollment"] = enrollment;
                Response.Redirect("ConfirmationPage.aspx");
            }
            else
            {
                switch (error)
                {
                    case ErrorMesages.UnableToCompleteRequest:
                        {
                            var script = "ShowError('" + error + "')";
                            Page.ClientScript.RegisterStartupScript(typeof(string), "Error", script, true);
                        }; break;
                    case ErrorMesages.DuplicateKit:
                        {
                            var script = "ShowErrorDuplicate('" + error + "')";
                            Page.ClientScript.RegisterStartupScript(typeof(string), "Error", script, true);
                        }; break;
                }
                //  lbError.Text = error;
                //mpe.Show();
                //errorPanel.Visible = true;
            }
        }



        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("EnrollmentRequest.aspx");
        }

        protected void lstShippingOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstShippingOptions.SelectedValue == "O")
            {
                pnlOtherShippingAddress.Visible = true;
            }
            else
            {
                pnlOtherShippingAddress.Visible = false;
            }
        }

        protected void ddlWaterHeaterFuel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlWaterHeaterFuel.SelectedValue == "Electric")
            {
                pnlKit.Visible = true;
            }
            else
            {
                pnlKit.Visible = false;
            }
        }

        protected void ddlHeaterFuel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlReferralSources.Focus();
        }

        protected void Email_OnChange(object sender, EventArgs e)
        {
            pnlMoreInfo.Visible = !string.IsNullOrEmpty(txtEmail.Text);
            txtConfirmEmail.Focus();
        }

        public void VerifyWaterHeater()
        {
            if (ddlWaterHeaterFuel.SelectedValue == "Electric" || ddlWaterHeaterFuel.SelectedValue == "Non-Electric")
            {
                pnlMoreInfo.Visible = false;
            }
        }
    }
}