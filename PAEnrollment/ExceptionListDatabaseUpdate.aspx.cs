﻿using CFLKits.Data.Presenters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PAEnrollment
{
    public partial class ExceptionListDatabaseUpdate : System.Web.UI.Page
    {
        EnrollmentsPresenter presenter = new EnrollmentsPresenter();
        String connectionString = ConfigurationManager.ConnectionStrings["CFLKitsDb"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void UploadButton(object sender, EventArgs e)
        {
            UploadedFile file = null;

            foreach (string fileInputID in Request.Files)
            {
                file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);

                if (file.ContentLength > 0 && file.GetExtension() == ".csv")
                {
                    int rowsNo;
                    int errNo;
                    string operationType = RadioButtonList.SelectedValue;
                    //replace
                    if (string.Equals(operationType, "Replace", StringComparison.InvariantCultureIgnoreCase))
                    {
                        file.SaveAs(Server.MapPath("~/csvUpload/") + file.FileName);
                        string filePath = Server.MapPath("~/csvUpload/") + file.FileName;

                        if (UpdateTable(filePath, operationType, out rowsNo, out errNo))
                        {
                            InsertFileInfoInExceptionDatabaseUploadHistory(filePath, operationType, rowsNo, errNo);
                            Response.Redirect("~/InvitationCodeUpdateStatusExceptionPage.aspx?No=" + rowsNo + "&ErrNo=" + errNo + "&Status=S");
                        }
                        else
                        {
                            Response.Redirect("~/InvitationCodeUpdateStatusExceptionPage.aspx?No=0&Status=F");
                        }
                    }
                    //append
                    else if (string.Equals(operationType, "Append", StringComparison.InvariantCultureIgnoreCase))
                    {
                        file.SaveAs(Server.MapPath("~/csvUpload/") + file.FileName);
                        string filePath = Server.MapPath("~/csvUpload/") + file.FileName;

                        if (UpdateTable(filePath, operationType, out rowsNo, out errNo))
                        {
                            InsertFileInfoInExceptionDatabaseUploadHistory(filePath, operationType, rowsNo, errNo);
                            Response.Redirect("~/InvitationCodeUpdateStatusExceptionPage.aspx?No=" + rowsNo + "&ErrNo=" + errNo + "&Status=S");
                        }
                        else
                        {
                            Response.Redirect("~/InvitationCodeUpdateStatusExceptionPage.aspx?No=0&Status=F");
                        }

                    }
                    else
                    {
                        lblError.Text = "Please select an upload mode: append or replace";
                    }
                }
                else
                {
                    lblError.Text = "Please select a CSV file to upload";
                }
            }
        }

        private bool UpdateTable(string path, string operation, out int rowsNo, out int errNo)
        {
            bool SuccessMode = false;
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn());
            //dt.Columns.Add(new DataColumn());
            //dt.Columns.Add(new DataColumn());
            //dt.Columns.Add(new DataColumn());
            //dt.Columns.Add(new DataColumn());
            //dt.Columns.Add(new DataColumn());


            string[] csvRows = File.ReadAllLines(path);
            var totalRows = csvRows.Count() - 1;
            rowsNo = 0;
            errNo = 0;

            var rules = new List<Rule<string[]>> 
            {
                new Rule<string[]> 
                {
                    RuleName = "MaxLength50", 
                    Message = "Length must be smaller than 50", 
                    Test = s=> s.All(el=>el.Length <= 50)
                },
                new Rule<string[]> 
                    {
                    RuleName="NotNull", 
                    Message = "Field must not be null", 
                    Test = s=> !string.IsNullOrEmpty(s[0])// && !string.IsNullOrEmpty(s[2]) && !string.IsNullOrEmpty(s[3]) 
                    }
            };

            for (int i = 1; i <= totalRows; i++)
            {
                var currentRow = csvRows[i];
                var fields = currentRow.Split(',');
                bool validRow = rules.All(rule => rule.Test(fields));
                if (!validRow)
                {
                    errNo++;
                }
                else
                {
                    DataRow row = dt.NewRow();
                    row.ItemArray = fields;
                    dt.Rows.Add(row);
                    rowsNo++;
                }
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlTransaction transaction;
                SqlCommand command;
                connection.Open();
                transaction = connection.BeginTransaction();

                try
                {
                    if (string.Equals(operation, "replace", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int rowsDeleted = new SqlCommand("delete from Exception", connection, transaction).ExecuteNonQuery();
                    }

                    //create temp table
                    string sqlQueryCreateTmpException = "CreateTmpException";
                    command = new SqlCommand(sqlQueryCreateTmpException, connection, transaction);
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();

                    //insert all distinct data from file in temp table
                    using (SqlBulkCopy bulkcopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                    {
                        bulkcopy.DestinationTableName = "TmpException";
                        bulkcopy.WriteToServer(dt);
                    }

                    //copy distinct data to original table
                    string sqlCopyDistinct = "dbo.InsertDistinctException";
                    command = new SqlCommand(sqlCopyDistinct, connection, transaction);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 60;
                    int queryResult = command.ExecuteNonQuery();

                    rowsNo = queryResult;
                    errNo = totalRows - rowsNo;

                    transaction.Commit();
                    SuccessMode = true;
                }
                catch
                {
                    transaction.Rollback();
                    SuccessMode = false;
                }
                finally
                {
                    //delete temp table
                    try
                    {
                        string sqlQueryDeleteTmpException = "dbo.DropTableTmpException";
                        command = new SqlCommand(sqlQueryDeleteTmpException, connection, transaction);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                    }
                    catch { }
                    connection.Close();
                }
            }

            return SuccessMode;
        }

        private void InsertFileInfoInExceptionDatabaseUploadHistory(string path, string operation, int rowsNo, int errNo)
        {
            var action = string.Empty;
            if (string.Equals(operation, "Append", StringComparison.InvariantCultureIgnoreCase))
            {
                action = "A";
            }
            else
            {
                action = "R";
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlTransaction transaction;
                connection.Open();
                transaction = connection.BeginTransaction();

                try
                {
                    SqlCommand cmd = new SqlCommand("dbo.InsertToExceptionDatabaseUploadHistory", connection, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@uploadDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@fileName", path.Split('\\').Last());
                    cmd.Parameters.AddWithValue("@action", action);
                    cmd.Parameters.AddWithValue("@recordsUploaded", rowsNo);
                    cmd.Parameters.AddWithValue("@exceptions", errNo);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        protected void CancelButton(object sender, EventArgs e)
        {
            Response.Redirect("Admin/ExceptionListDatabaseUpdateHistoryPage.aspx");
        }
    }
}