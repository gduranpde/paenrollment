﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.ascx.cs" Inherits="PAEnrollment.Cards.UploadFile" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />
<table>
    <tr>
        <td>
            Upload date :
        </td>
          <td>
            <telerik:RadDatePicker ID="RadDatePicker_SubmittedDate" Runat="server" Skin="Telerik" Width="120px">
                <calendar skin="Telerik" usecolumnheadersasselectors="False" 
                    userowheadersasselectors="False" viewselectortext="x" runat="server">
                </calendar>
                <datepopupbutton hoverimageurl="" imageurl="" runat="server" />
                <dateinput dateformat="MM/dd/yyyy" displaydateformat="MM/dd/yyyy" runat="server">
                </dateinput>
            </telerik:RadDatePicker>
        </td>       
    </tr>
    <tr>
         <td>
            File name :
        </td>
        <td>
            <asp:TextBox ID="txtFileName" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
           Replace/Append :
        </td>
        <td>
            <asp:TextBox ID="txtReplaceOrAppend" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            Records Uploaded :
        </td>
        <td>
            <asp:TextBox ID="txtRecordsUploaded" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            Exceptions :
        </td>
        <td>
            <asp:TextBox ID="txtExceptions" runat="server"></asp:TextBox>
        </td>
    </tr>  
</table>