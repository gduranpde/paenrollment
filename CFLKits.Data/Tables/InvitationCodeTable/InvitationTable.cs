﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CFLKits.Data.Entities;
using CFLKits.Data.Filters;
using CFLKits.Entities;
using Dims.DAL;

namespace CFLKits.Data.Tables
{
    [spGetMany(typeof(sp_InvitationCodes_GetMany))]
    [spUpload(typeof(sp_CSVtoTable))]
    public class InvitationTable : Table<Invitation_Code>
    {
        public List<Invitation_Code> GetByAccountNumberOrInvCode(InvitationCodeFilter filter)
        {
            return db.Execute<Invitation_Code>(new sp_InvitationCodes_GetMany { InvitationCode = filter.InvitationCode, AccountNumber = filter.AccountNumber, AccountNumber12 = filter.AccountNumber12 });
        }
        //public void UploadExceptionHistoryTable(string path)
        //{
        //    db.Execute(new sp_ExceptionHistory_Add{ path});
        //}
    }
}