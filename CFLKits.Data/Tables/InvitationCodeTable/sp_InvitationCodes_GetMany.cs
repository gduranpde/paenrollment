﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace CFLKits.Data.Tables
{
    public class sp_InvitationCodes_GetMany : StoredProcedure
    {
        [InParameter]
        public string InvitationCode;

        [InParameter]
        public string AccountNumber;

        [InParameter]
        public string AccountNumber12;
    }
}
