﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace CFLKits.Data.Tables.ExceptionTable
{
    public class sp_GetAccountNumberException : StoredProcedure
    {
       [InParameter]
       public string AccountNumber;
    }
}
