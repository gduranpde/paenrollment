﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CFLKits.Data.Tables;
using CFLKits.Data.Tables.ExceptionTable;
using Dims.DAL;

namespace CFLKits.Data
{
    //[spGetAccountNumberException(typeof(sp_GetAccountNumberException))]
    public class ExceptionTable : Table<Exception>
    {
        public int GetAccException(string accountException)
        {
            return db.ExecuteScalar<int>(new sp_GetAccountNumberException { AccountNumber = accountException });
        }
    }
}
