
using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;

using  CFLKits.Data.Tables;
using CFLKits.Data.Entities;


namespace  CFLKits.Data
{
    public class CFLKitsDb : DataBase
    {

        public EnrollmentTable Enrollments = new EnrollmentTable();
        public ExceptionTable Exception = new ExceptionTable();
        public InvitationTable InvitationCodes = new InvitationTable();
        public UploadFile UploadFiles = new UploadFile();
    }
}

