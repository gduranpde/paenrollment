﻿
namespace CFLKits.Data.Entities
{
    public class Invitation_Code
    {
        public string InvitationCode { get; set; }
        public string AccountNumber { get; set; }
        public string AccountNumber12 { get; set; }
        public string MailingZIP { get; set; }
        public string ServiceZIP { get; set; }
        public string OperatingCompany { get; set; }
    }
}
