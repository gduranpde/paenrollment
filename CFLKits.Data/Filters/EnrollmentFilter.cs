﻿using System;

namespace CFLKits.Data.Filters
{
    public class EnrollmentFilter
    {
        public string InvitationCode { get; set; }


        public string AccountNumber { get; set; }

        public string AccountNumber12 { get; set; }


        public DateTime? FromDate { get; set; }


        public DateTime? ToDate { get; set; }


        public string ContactFirstName { get; set; }

        public string ContactLastName { get; set; }


        public string ContactEmail { get; set; }

    }
}
