﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFLKits.Data.Filters
{
    public class UploadFileFilter
    {
        public int UploadID { get; set; }
        public DateTime UploadDate { get; set; }
        public string FileName { get; set; }
        public string ReplaceOrAppend {get;set;}
        public int RecordsUploaded {get;set;}
        public int Exceptions {get;set;}
    }
}
