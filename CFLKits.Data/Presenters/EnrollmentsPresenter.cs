﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CFLKits.Data.Entities;
using CFLKits.Data.Filters;
using CFLKits.Entities;
using System.Configuration;

namespace CFLKits.Data.Presenters
{
    public enum ErrorMesages
    {
        NoError,
        UnableToCompleteRequest,
        DuplicateKit
    }
    public class EnrollmentsPresenter
    {

        public List<Enrollment> GetFiltered(EnrollmentFilter filter)
        {
            List<Enrollment> l = DataProvider.Current.Enrollments.GetMany(filter);
            return l;
        }

        public ErrorMesages SaveEnrollment(Enrollment enrollment, bool isAdmin)
        {
            var ret = ErrorMesages.NoError;
            if (!isAdmin)
            {
                InvitationCodeFilter invitationCodeFilter = new InvitationCodeFilter();
                invitationCodeFilter.AccountNumber = enrollment.AccountNumber;
                invitationCodeFilter.AccountNumber12 = enrollment.AccountNumber12;
                invitationCodeFilter.InvitationCode = enrollment.InvitationCode;

                if (invitationCodeFilter.AccountNumber == "")
                    invitationCodeFilter.AccountNumber = null;

                if (invitationCodeFilter.AccountNumber12 == "")
                    invitationCodeFilter.AccountNumber12 = null;

                if (invitationCodeFilter.InvitationCode == "")
                    invitationCodeFilter.InvitationCode = null;

                List<Invitation_Code> codes = DataProvider.Current.InvitationCodes.GetByAccountNumberOrInvCode(invitationCodeFilter);

                if (!string.IsNullOrEmpty(enrollment.InvitationCode))
                {
                    if (codes.Count == 0)
                        return ErrorMesages.UnableToCompleteRequest;

                    bool zipIsOk = false;
                    foreach (Invitation_Code invitationCode in codes)
                    {
                        if (invitationCode.MailingZIP == enrollment.ContactZipCode || invitationCode.ServiceZIP == enrollment.ContactZipCode)
                            zipIsOk = true;
                    }

                    if (!zipIsOk)
                        return ErrorMesages.UnableToCompleteRequest;
                }

                if (!string.IsNullOrEmpty(enrollment.AccountNumber) || !string.IsNullOrEmpty(enrollment.AccountNumber12))
                {
                    if (codes.Count == 0)
                        return ErrorMesages.UnableToCompleteRequest;

                    bool zipIsOk = false;
                    foreach (Invitation_Code invitationCode in codes)
                    {
                        if (invitationCode.MailingZIP == enrollment.ContactZipCode || invitationCode.ServiceZIP == enrollment.ContactZipCode)
                            zipIsOk = true;
                    }

                    if (!zipIsOk)
                        return ErrorMesages.UnableToCompleteRequest;
                }

                //EnrollmentFilter ef = new EnrollmentFilter()
                //                          {
                //                              AccountNumber = enrollment.AccountNumber,
                //                              InvitationCode = enrollment.InvitationCode
                //                          };

                //List<Enrollment> list = DataProvider.Current.Enrollments.GetMany(ef);
                //if (list.Count > 0)
                //    return "Already created with such Invitation code or Account #.";

                if (codes.Count > 0)
                {
                    foreach (Invitation_Code invitationCode in codes)
                    {
                        enrollment.AccountNumber = invitationCode.AccountNumber;
                        enrollment.AccountNumber12 = invitationCode.AccountNumber12;
                        enrollment.InvitationCode = invitationCode.InvitationCode;
                        enrollment.OperatingCompany = invitationCode.OperatingCompany;
                    }
                }

                //check for AccNo in Exception Table
                string accNbException = string.Empty;
                if (enrollment.AccountNumber != null)
                {
                    accNbException = enrollment.AccountNumber;
                }
                else
                {
                    accNbException = enrollment.AccountNumber12;
                }
                int existsAccNr = GetAccountException(accNbException);

                string accountExcNumber = string.Empty;
                if (invitationCodeFilter.AccountNumber != null)
                {
                    accountExcNumber = invitationCodeFilter.AccountNumber;
                }
                else
                {
                    accountExcNumber = invitationCodeFilter.AccountNumber12;
                }
                int isAccountExc = GetAccountException(accountExcNumber);

                if (existsAccNr > 0 || isAccountExc > 0)
                {
                    return ErrorMesages.DuplicateKit;
                }

                DataProvider.Current.Enrollments.Add(enrollment);
            }
            else
                DataProvider.Current.Enrollments.Save(enrollment);

            return ret;
        }
        
        public void Delete(Int64 id)
        {
            DataProvider.Current.Enrollments.Remove(id);
        }

        public Enrollment GetById(Int64 id)
        {
            return DataProvider.Current.Enrollments.GetOne(id);
        }

        public void UploadTable(string path)
        {
            DataProvider.Current.InvitationCodes.Upload(path);
        }

        //public void UploadExceptionHistoryTable(string path)
        //{
        //    DataProvider.Current.InvitationCodes.UploadExceptionHistoryTable(path);
        //}

        public int GetInvitationRecordCount()
        {
            InvitationCodeFilter invitationCodeFilter = new InvitationCodeFilter();
            invitationCodeFilter.AccountNumber = null;
            invitationCodeFilter.AccountNumber12 = null;
            invitationCodeFilter.InvitationCode = null;

            List<Invitation_Code> codes = DataProvider.Current.InvitationCodes.GetByAccountNumberOrInvCode(invitationCodeFilter);

            return codes.Count;
        }

        public void UpdateReplicationStatus(Int64 EnrollmentID)
        {
            DataProvider.Current.Enrollments.Update(EnrollmentID);
        }

        public List<Enrollment> GetEnrollmentID(EnrollmentDetails filter)
        {
            List<Enrollment> l = DataProvider.Current.Enrollments.GetEnrollID(filter);
            return l;
        }

        public int GetAccountException(string account)
        {
            var result = DataProvider.Current.Exception.GetAccException(account);
            return result;
        }
    }
}